﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Entities.Dtos
{
    public class TagDto
    {
        public string TagId { get; set; }

        public string TagName { get; set; }

        public string BGColor { get; set; }
    }
}
