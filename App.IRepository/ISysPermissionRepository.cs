﻿using App.Core;
using App.Entities;

namespace App.IRepository
{
    public interface ISysPermissionRepository : IBaseRepository<SysPermission>
    {

    }
}
